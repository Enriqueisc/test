## Iniciar el script

1. Tendrás que tener instalado NodeJS.
2. Instalar las dependencia con -> npm install
3. Para ejecutar el script necesitas el comando node app.js
4. Si requieres modificar las iteraciones en la linea 41 Divisible(10).
5. Si deseas agregar nuevas reglas deberas agregar un objeto {number: 3, string: "Foo"}

---

## Probar el script con JEST

Ahora, Para realizar las pruebas unitarias requieres JEST.

1. Tendrás que tener Jest instalado para las pruebas unitarias.
2. Si quieres modificar el test se encuentra en la carpeta __Test__.
3. Ejecutar -> npm test.

---
