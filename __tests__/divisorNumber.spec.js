let divisor = require('../app')
describe("Divisor 3,5,7", () => {
  test("Test Console", () => {
    const input = [
      {limit: 10 , rules: [{number: 3, string: "Foo"},{number: 5, string: "Bar"},{number: 7, string: "Qix"}]},
      {limit: 20 , rules: [{number: 3, string: "Foo"},{number: 5, string: "Bar"}]},
      {limit: 100 , rules: [{number: 2, string: "Kike"},{number: 8, string: "Sanz"}]}
    ];
    input.map((i) => {
      expect(divisor(i.limit, i.rules))
    });
  });
});
