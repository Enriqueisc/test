
const divisible = (string, rules) => {
    value = "";
    rules.map((rule) => {value += parseInt(string) % rule.number === 0  ? rule.string  : ""});
    for (i = 0; i < string.length; i++) {
        char = string.charAt(i);
        rules.map((rule) => {
            if (char === '0'){
                string = string.replace(/0/g, '*');
            }else{
                let valor = char === rule.number.toString() ? rule.string  : ""
                value +=  valor
                value = value
            }
        })
        if (value.match(/[a-zA-Z]/)) {
            value += char.replace(/0/g, '*').replace(/[1-9]/g, '')
            value = value
        }
    }
    return value == ""  ? `${string}` : `${value}`
}

function Divisible(number, rules){
    for (index = 1; index <= number; index++) {
        let string = `${index}`
        let valor =  divisible(string, rules) 
        var lista = []

        rules.map((rule) => {
            if (index % rule == 0) {
                lista.push('divisible by', rule.number)
            }
        });
        console.log(`${valor} ${ lista.length > 0  ? `(${lista.join(', ')})`: "" }`)
    }
}
/*var rules = [{number: 3, string: "Foo"},{number: 5, string: "Bar"},{number: 7, string: "Qix"}]
var limit = 10
Divisible(limit, rules)*/

module.exports =  Divisible
